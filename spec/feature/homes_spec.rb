require 'rails_helper'

RSpec.feature 'potepanトップページ' do
  background do
    visit potepan_root_path
  end

  feature 'header' do
    scenario '検索バーが表示されている' do
      expect(page).to have_selector('.searchBox')
    end
  end
end
