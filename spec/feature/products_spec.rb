require 'rails_helper'

RSpec.feature '商品詳細ページ' do
  given(:taxonomy_category) { create(:taxonomy, name: 'Category') }
  given(:taxonomy_brand) { create(:taxonomy, name: 'Brand') }
  given(:category_taxon) { taxonomy_category.root }
  given(:brand_taxon) { taxonomy_brand.root }
  given(:bag_taxon) { create(:taxon, name: 'Bag', taxonomy: taxonomy_category, parent: category_taxon) }
  given(:rails_taxon) { create(:taxon, name: 'Rails', taxonomy: taxonomy_brand, parent: brand_taxon) }
  given(:rails_tote) { create(:product, name: 'Rails-Tote', description: 'very very normal', taxons: [bag_taxon, rails_taxon]) }

  background { visit potepan_product_path(rails_tote.id) }

  scenario 'potepan/homeへのリンクが機能している' do
    within('.navbar') do
      click_on 'トップページ'
      expect(current_path).to eq potepan_root_path
      click_link 'Home'
      expect(current_path).to eq potepan_root_path
    end
  end

  scenario 'light_sectionが正しく表示される(SHOPは表示されない)' do
    expect(page).to have_selector '.lightSection .page-title', text: rails_tote.name
    expect(page).not_to have_selector '.lightSection .breadcrumb li:nth-child(2)', text: 'SHOP'
    expect(page).to have_selector '.lightSection .active', text: rails_tote.name
  end

  feature '商品画像・概要' do
    scenario '商品名、価格、商品紹介文が表示される' do
      expect(page).to have_title 'Rails-Tote - BIGBAG Store'
      expect(page).to have_selector 'h2', text: 'Rails-Tote'
      expect(page).to have_selector '.col-xs-6 .active', text: 'Rails-Tote'
      expect(page).to have_selector '.product_price', text: '$19.99'
      expect(page).to have_selector '.product_description', text: 'very very normal'
    end

    context 'productにtaxonが紐付いている場合' do
      scenario '一覧に戻るをクリックするとその商品のカテゴリ一覧に飛ぶ' do
        click_on '一覧ページへ戻る'
        expect(current_path).to eq potepan_category_path(rails_tote.taxons.first.id)
      end
    end

    context 'productにtaxonが紐付いてない場合' do
      background do
        rails_tote.taxons.destroy_all
        visit current_path
      end

      scenario '一覧に戻るをクリックするとSpree::Taxon.firstであるcategory_taxonのカテゴリ一覧に飛ぶ' do
        click_on '一覧ページへ戻る'
        expect(current_path).to eq potepan_category_path(category_taxon.id)
      end
    end
  end

  feature '関連商品' do
    given(:mug_taxon) { create(:taxon, name: 'Mug', taxonomy: taxonomy_category, parent: category_taxon) }
    given(:ruby_taxon) { create(:taxon, name: 'Ruby', taxonomy: taxonomy_brand, parent: brand_taxon) }
    given!(:rails_bag) { create(:product, name: 'Rails-Bag', taxons: [bag_taxon, rails_taxon]) }
    given!(:ruby_bag) { create(:product, name: 'Ruby-Bag', taxons: [bag_taxon, ruby_taxon]) }
    given!(:rails_mug) { create(:product, name: 'Rails-Mug', taxons: [mug_taxon, rails_taxon]) }
    given!(:ruby_mug) { create(:product, name: 'Ruby-Mug', taxons: [mug_taxon, ruby_taxon]) }
    given!(:related_products) { Spree::Product.related_products(rails_tote).includes_images_prices }

    background do
      visit current_path
    end

    scenario '関連した商品のみが重複なく表示される' do
      expect(page).to have_selector '.productsContent', text: 'Rails-Bag', count: 1
      expect(page).to have_selector '.productsContent', text: 'Ruby-Bag', count: 1
      expect(page).to have_selector '.productsContent', text: 'Rails-Mug', count: 1
      within('.productsContent') { expect(all('.productBox').size).to eq(3) }
    end

    scenario '関連元の商品は表示されない' do
      expect(page).not_to have_selector '.productsContent', text: 'Rails-Tote'
    end

    scenario '商品情報が正しく表示される' do
      related_products.each do |r_product|
        expect(page).to have_selector '.productsContent', text: r_product.name
        expect(page).to have_selector '.productsContent', text: r_product.display_price
      end
    end

    scenario '関連商品をクリックすると該当の商品詳細ページへ移動' do
      find('.productsContent').click_on ruby_bag.name
      expect(current_path).to eq potepan_product_path(ruby_bag.id)
    end
  end
end
