require 'rails_helper'

RSpec.feature 'Categoriesビュー' do
  given(:taxonomy_category) { create(:taxonomy, id: 1, name: 'Category') }
  given(:taxonomy_brand) { create(:taxonomy, id: 2, name: 'Brand') }
  given(:category_taxon) { taxonomy_category.root }
  given(:brand_taxon) { taxonomy_brand.root }
  given(:bag) { create(:taxon, name: 'bag', taxonomy: taxonomy_category, parent: category_taxon) }
  given(:rails) { create(:taxon, name: 'rails', taxonomy: taxonomy_brand, parent: brand_taxon) }
  given!(:rails_tote) { create(:product, name: 'Rails-Tote', taxons: [bag, rails]) }
  given!(:rails_cup) { create(:product, name: 'Rails-Cup', taxons: [rails]) }

  background do
    visit potepan_category_path(category_taxon.id)
  end

  feature '表示内容' do
    scenario 'light_sectionが正しく表示されている' do
      expect(page).to have_selector '.lightSection .page-title', text: category_taxon.name
      expect(page).to have_selector '.lightSection .breadcrumb li:nth-child(2)', text: 'SHOP'
      expect(page).to have_selector '.lightSection .active', text: category_taxon.name
    end

    feature 'taxonomies_sidbar' do
      given(:taxonomies) { Spree::Taxonomy.includes(:root) }

      scenario 'taxonomiesが全て表示されている' do
        taxonomies.each { |taxonomy| expect(page).to have_selector '.taxonomies_sidebar', text: taxonomy.name }
      end

      scenario 'taxonomyをそれぞれクリックすると属するtaxonが表示される' do
        taxonomies.each do |taxonomy|
          find(".sideBar>.panel:nth-child(1)").click_on taxonomy.name
          taxonomy.root.leaves.each { |taxon| expect(page).to have_selector '.sideBar>.panel:nth-child(1)', text: taxon.name }
        end
      end
    end

    feature 'category_products' do
      context 'taxon = Categoryの場合' do
        scenario 'categoryの商品が全て表示される' do
          expect(find(".category-products")).to have_content('Rails-Tote', count: 1)
          within('.category-products') { expect(all('.partial_product').size).to eq(1) }
        end
      end

      context 'taxon = Railsの場合' do
        background do
          visit potepan_category_path(rails.id)
        end
        # リクエストが成功した際に各taxonの情報が含まれているかどうかのテストは、request_testに移動しました
        scenario 'railsの商品が全て表示される' do
          expect(find(".category-products")).to have_content('Rails-Tote', count: 1)
          expect(find(".category-products")).to have_content('Rails-Cup', count: 1)
          within('.category-products') { expect(all('.partial_product').size).to eq(2) }
        end
      end
    end
  end

  feature '画面内のリンク' do
    scenario '商品クリックで商品詳細画面へ移動' do
      click_on rails_tote.name
      expect(current_path).to eq potepan_product_path(rails_tote.id)
    end
  end
end
