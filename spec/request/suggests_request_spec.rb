require 'rails_helper'
require 'webmock'

describe Potepan::HomesController, type: :request do
  describe 'GET/SUGGEST' do
    before do
      WebMock.stub_request(:get, "presite-potepanec-task5.herokuapp.com/potepan/api/suggests").
        with(
          headers: { 'Authorization' => 'Bearer api123' },
          query: hash_including({ :keyword => "ruby", :max_num => "3" })
        ).
        to_return(
          body: ['ruby', 'ruby for women', 'ruby for men'].to_json,
          status: 200,
          headers: { 'Content-Type' => 'application/json' }
        )
    end

    it 'リクエストが成功する' do
      get '/potepan/suggest'
      expect(response.status).to eq(200) # webmockを通さず素のレスポンスステータスを確認
    end

    it 'レスポンスにrubyを含んだ検索候補を3個返す' do
      get '/potepan/suggest', params: { keyword: 'ruby', max_num: 3 }
      expect(JSON.parse(response.body)).to eq ['ruby', 'ruby for women', 'ruby for men'] # webmockを通しresponse.bodyを確認
    end
  end
end
