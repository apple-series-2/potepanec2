require 'rails_helper'

describe Potepan::CategoriesController, type: :request do
  shared_examples_for 'リクエストが成功する' do
    it { expect(response.status).to eq(200) }
  end

  describe "GET /show" do
    subject { response.body }

    let(:category_taxon) { create(:taxon, name: 'Category') }
    let(:bag) { create(:taxon, name: 'Bag', parent: category_taxon) }
    let(:html) { Nokogiri::HTML(response.body) }
    let(:lightSection_doc) { html.css('.lightSection li:last-child') }

    context 'taxonにCategoryを渡した場合' do
      before do
        get potepan_category_path(category_taxon.id)
      end

      it_behaves_like 'リクエストが成功する'

      it 'リクエストにcategory_taxonが含まれている' do
        expect(lightSection_doc.to_s).to include 'Category'
      end

      it 'logoSlider,pageHeader,header,footerが表示されている' do
        ['partnersLogoSlider', 'pageHeader', 'header', 'footer']. each { |tag| is_expected.to include tag }
      end
    end

    context 'taxonにbagを渡した場合' do
      before do
        get potepan_category_path(bag.id)
      end

      it_behaves_like 'リクエストが成功する'

      it 'リクエストにbagが含まれている' do
        expect(lightSection_doc.to_s).to include 'Bag'
      end
    end
  end
end
