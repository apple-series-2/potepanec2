require 'rails_helper'

describe Potepan::Api::V1::ApiSuggestsController, type: :request do
  describe 'GET /api/v1/suggest' do
    let!(:keyword1) { create(:suggest, keyword: 'ruuuuby') }
    let!(:keyword2) { create(:suggest, keyword: 'ruby') }
    let!(:keyword3) { create(:suggest, keyword: 'ruby bag') }
    let!(:keyword4) { create(:suggest, keyword: 'ruby cup') }
    let(:headers) { { 'Authorization' => "Bearer #{Rails.application.credentials.myapi[:suggest_myapi_key]}" } }

    it 'リクエストが成功する' do
      get '/potepan/api/v1/suggest', params: { keyword: '' }, headers: headers
      expect(response.status).to eq(200)
    end

    it '検索候補をmax_numの数だけ取得できる' do
      get '/potepan/api/v1/suggest', params: { keyword: 'ruby', max_num: 2 }, headers: headers
      json = JSON.parse(response.body)
      expect(json.size).to eq 2
      expect(json[0]).to eq keyword2[:keyword]
      expect(json[1]).to eq keyword3[:keyword]
    end

    it 'クエリに:keywordが存在しない場合500エラーを返す' do
      get '/potepan/api/v1/suggest', headers: headers
      expect(response.status).to eq(500)
    end

    it '認証キーを間違えた場合401エラーを返す' do
      get '/potepan/api/v1/suggest', headers: { 'Authorization' => 'Bearer misspass' }
      expect(response.status).to eq(401)
    end
  end
end
