require 'rails_helper'

describe Potepan::HomesController, type: :request do
  describe 'GET/INDEX' do
    before do
      get potepan_root_path
    end

    it 'リクエストが成功する' do
      expect(response.status).to eq(200)
    end
  end
end
