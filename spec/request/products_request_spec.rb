require 'rails_helper'

describe Potepan::ProductsController, type: :request do
  describe 'GET /show' do
    subject { response.body }

    let(:bag) { create(:taxon, name: 'Bag') }
    let(:rails_tote) { create(:product, name: 'Rails-Tote', description: 'very very normal', taxons: [bag]) }
    let!(:related_products) { create_list(:product, 5, name: 'Related-Product', taxons: [bag]) }
    let(:html) { Nokogiri::HTML(response.body) }
    let(:productBox_doc) { html.css('.productsContent .productBox') }

    before do
      get potepan_product_path(rails_tote.id)
    end

    it 'リクエストが成功し、渡したproductが正しくresponseに含まれている' do
      expect(response.status).to eq(200)
      ['Rails-Tote', 'very very normal'].each { |word| is_expected.to include word }
    end

    it 'pageHeader,header,footerが表示されている' do
      ['pageHeader', 'header', 'footer'].each { |tag| is_expected.to include tag }
    end

    it '最大4つまで取得した関連商品が全て表示されている' do
      is_expected.to include 'Related-Product'
      expect(productBox_doc.size).to eq(4)
    end
  end
end
