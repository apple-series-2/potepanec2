require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_titleメソッドテスト" do
    let(:base_title) { 'BIGBAG Store' }

    context "タイトルが入力されている場合" do
      it "[タイトル - BIGBAG Store]が表示される" do
        expect(full_title("test")).to eq "test - #{base_title}"
      end
    end

    context "タイトルがnilの場合" do
      it "[BIGBAG Store]が表示される" do
        expect(full_title(nil)).to eq "#{base_title}"
      end
    end

    context "タイトルが空の場合" do
      it "[BIGBAG Store]が表示される" do
        expect(full_title("")).to eq "#{base_title}"
      end
    end
  end
end
