class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.all_products.includes_images_prices.page(params[:page])
    @taxonomies = Spree::Taxonomy.includes(:root)
  end
end
