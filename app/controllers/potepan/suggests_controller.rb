require 'httpclient'

class Potepan::SuggestsController < ApplicationController
  def suggest
    client = HTTPClient.new

    uri = Rails.application.credentials.api[:suggest_api_url]

    header = { "Authorization" => "Bearer #{Rails.application.credentials.api[:suggest_api_key]}" }

    query = {
      "keyword" => params[:keyword],
      "max_num" => params[:max_num],
    }

    response = client.get(uri, query, header)

    if response.status == 200
      render json: JSON.parse(response.body)
    else
      render json: [], status: response.status
    end
  end
end
