class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_MAXIMUM = 4

  def show
    @product = Spree::Product.find(params[:id])
    @link_taxon = @product.taxons.present? ? @product.taxons.first : Spree::Taxonomy.first.root
    @related_products = Spree::Product.related_products(@product).limit(RELATED_PRODUCTS_MAXIMUM).includes_images_prices
  end
end
