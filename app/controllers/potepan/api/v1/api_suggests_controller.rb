module Potepan
  module Api
    module V1
      class ApiSuggestsController < ApiController
        include ActionController::HttpAuthentication::Token::ControllerMethods

        before_action :authenticate

        def suggest
          if params[:keyword]
            suggests = Potepan::Suggest.search_by_keyword(params[:keyword]).limit(params[:max_num]).pluck(:keyword)
            render json: suggests
          else
            head :internal_server_error
          end
        end
      end
    end
  end
end
