module Potepan
  module Api
    class ApiController < ApplicationController
      layout "error_layout"

      rescue_from StandardError, with: :rescue500

      protected

      def rescue500(e)
        logger.info "Rendering 500 with exception: #{e.message}" if e
        head :internal_server_error
      end

      def authenticate
        authenticate_token || render_unauthorized
      end

      def authenticate_token
        authenticate_with_http_token do |token, options|
          myapi_key = Rails.application.credentials.myapi[:suggest_myapi_key]
          ActiveSupport::SecurityUtils.secure_compare(token, myapi_key)
        end
      end

      def render_unauthorized
        head :unauthorized
      end
    end
  end
end
