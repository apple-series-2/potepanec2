Spree::Product.class_eval do
  scope :includes_images_prices, -> { includes(master: [:default_price, :images]) }
  scope :related_products, -> (product) {
    in_taxons(product.taxons).where.not(id: product.id).distinct
  }
end
