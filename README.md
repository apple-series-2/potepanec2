# BIGBAG
***

"BIGBAG"は、プログラミングスクール ポテパン内の課題において作成したECサイトです。



##  開発環境
***
* MacOS（Catalina）
* Ruby
* Rails
* VSCode（Visual Studio Code）
* Docker
* CircleCI
* Solidus
* Rspec
* jQuery
* bootstrap
* 本番環境：heroku



## 技術概要
***
* 環境構築
    * Dockerによる開発環境、CicleCIによるherokuデプロイ、Rubocop、Rspecの自動化
* Solidus
    * Solidusのモデル・ビュー・コントローラを基本に、各種レイアウトのカスタマイズ
    * 商品詳細ページ・関連商品を動的に表示
    * サイドバーのカテゴリーが動的に表示
   * 選択したカテゴリーに属する商品一覧を表示
* API
    * 外部APIを利用し、検索ボックスにサジェスト機能を追加
    * 逆に外部APIとして呼び出しができるよう、検索サジェストの候補一覧を返すAPIを作成
